#Policy Name: terraform_aws

#Read-only permission on "secret/terraform_aws/ path
path "secret/data/terraform_aws/access_key" {
    capabilities = [ "read" ]
}
