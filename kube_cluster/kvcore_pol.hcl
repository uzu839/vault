#Policy name: kvcore

#Read-only permission on "secret/kvcore/azure/spn" 
path "secret/data/kvcore/azure/spn" {
  capabilities = ["read"]
}
